## ☕ Projeto <java_exercises>

Exercícios práticos de Java do Núcleo de TI da Porto, neste conteúdo aprendemos:

```
 - Conceitos
 - Dados
 - Estruturas 
 - Classes e Objetos
 - Heranças

```


## 📝 Licença

Esse projeto está sob licença. Veja o arquivo [LICENÇA](LICENSE.md) para mais detalhes.

[⬆ Voltar ao topo](#nome-do-projeto)<br>
